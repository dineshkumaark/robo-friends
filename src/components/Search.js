import React from 'react';

const Search = (props)=>{
    
    return(
        <React.Fragment>
            <input type="text" placeholder="Enter Robot Name....." className="input-reset ba b--black-20 pa2 mb2 mt3" onChange={props.change}/>
        </React.Fragment>
    )
}

export default Search;