import React from 'react';
const Card = (props) =>{
    const {name,username,email} = props;
    return(
        <div className="bg-light-blue dib br3 pa3 ma2 grow bw2 shadow-5 card">
            <div className="roboimg">
                <img src={`https://robohash.org/${username}?set=set2`} alt="robo" width="200px" height="180px"/>
            </div>
            <div className="robodesc">
                <p>{name}</p>
                <p>{email}</p>
            </div>
        </div>
    )
}

export default Card;