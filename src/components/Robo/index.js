import React from 'react';
import Card from '../Card';
const Robo = (props)=>{
    const {robot} = props;
    return(
        <div style={{marginLeft: '3.5%'}}>
            {robot.map((e,i)=> <Card  username={e.username} name={e.name} email={e.email} key={i}/>)}
        </div>
    );
}

export default Robo;