import React from 'react';
import Search from './Search';
const Nav = (props) =>{
    return(
        <div className="ma3" style={{textAlign:'center'}}>
            <span className="brand light-green">ROBOFRIENDS</span><br/>
            <Search change={props.onchange}/>
        </div>
    )
}

export default Nav;