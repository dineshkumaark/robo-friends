import React ,{Component}from 'react';
import Robo from './components/Robo/';
import './App.css';
import 'tachyons';
import Nav from './components/Nav';
import {robots} from './components/Robo/robots'
class App extends Component {
  state ={
    robots: robots,
  }
  change = (e)=>{
    const {value} = e.target;
    const n = value.length;
    console.log(value.length);
    const {robots} = this.state;
    const newrobo = robots.filter((robo)=> robo.name.substring(0,n).toLowerCase() === value);
    if(newrobo.length  !== 0){
      this.setState({robots: newrobo});
    }
    if(value.length === 0 || newrobo.length === 0){
      fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(data => this.setState({robots: data}))
    }
    
    console.log(newrobo);
  }
  componentDidMount(){
     fetch('https://jsonplaceholder.typicode.com/users')
     .then(res => res.json())
     .then(data => this.setState({robots: data}))
  }
 render(){
   const { robots } = this.state;
  return (
    <div className="App">
      <Nav onchange={this.change}/>
      <Robo robot={robots}/>
    </div>
  );
 }
  
}

export default App;
